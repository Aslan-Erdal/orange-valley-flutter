import 'package:http/http.dart' as http;
import 'package:orange_valley_caa/models/video.dart';
import 'package:orange_valley_caa/utils/helper.dart';

Future<List<Video>> getVideosFromApi({ VideoSort filter = VideoSort.id }) async {
  // Await the http get response, then decode the json-formatted response.

  String url = "https://orangevalleycaa.org/api/videos/order/${filter.filterName()}";

  var response = await http.get(Uri.parse(url));

  if (response.statusCode == 200) {
    var json = response.body;
    return videoFromJson(json);
  } else {
    return [];
  }
}