import 'package:orange_valley_caa/models/video.dart';

List<Video> getFakeVideos() {
  return [
    Video(
        name: "Modeling Faces in Clay with Janet Blake",
        description:
            "This workshop, lead by acclaimed sculptor, and OVCAA resident artist, Janet Blake, is a don't miss experience. Janet shares tips and tricks that have made her one of the foremost artists in modeling faces in clay. Her engaging style will have you clamoring for more.",
        thumbnail:
            "http://orangevalleycaa.org/api/media/images/thumbs/WomanSculptsClay_042009083.png",
        keywords: "clay,face,janet blake,sculpture"),
    Video(
        name: "Fire Dancers Shine Bright",
        description:
            "Fire Rhythm, a Los Angeles dance team, is rising in the elite ranks of fire dancing. This video captured their astounding performance at the Orange Valley Contemporary Arts Museum.",
        thumbnail:
            "http://orangevalleycaa.org/api/media/images/thumbs/FireDancers_042981315.png",
        keywords: "baton,dance,fire,performer,poi"),
    Video(
        name: "Graphic Art Camp Draws Youth Artists in Droves",
        description:
            "Ben Woolrich, Principal Designer for Metro Design, has been teaching a Saturday graphic design camp at the Orange Valley Community Center. We filmed his recent session to share his inspiring lessons with the youth art community.  Ben's warm and open approach to education, along with his mastery of design, is a joy to behold.",
        thumbnail:
            "http://orangevalleycaa.org/api/media/images/thumbs/GraphicArtist_052214820.png",
        keywords: "computer,design,graphic,tablet,youth"),
    Video(
        name: "The Art of Welding",
        description:
            "Three Orange Valley metal sculptors share their talent, vision, and tips for turning a love of welding and sculpture into a hot career.",
        thumbnail:
            "http://orangevalleycaa.org/api/media/images/thumbs/SculptorWelding_053783433.png",
        keywords: "art,metal,sculpture,welding"),
    Video(
        name: "Leaps and Bounds",
        description:
            "A local ballet group known as \"Leaps and Bounds\", is wowing audiences with their highly stylized recitals performed against wildly animated backgrounds. Our OVCAA video team took a look at the artistry behind these shows and learned that there's more to it than smoke and mirrors.",
        thumbnail:
            "http://orangevalleycaa.org/api/media/images/thumbs/BalletInSmoke_054599628.png",
        keywords: "ballet,dance,performance,production,recital,smoke"),
    Video(
        name: "Blowing Glass",
        description:
            "Thai glass blowers create some of the most elaborate and dramatic sculptures, vases, and ornaments seen in this artistic genre. OVCAA video producer, Betty Clinton, was recently vacationing in Thailand and decided to turn her camera on this intriguing and mystifying art, and its talented artists.",
        thumbnail:
            "http://orangevalleycaa.org/api/media/images/thumbs/GlassBlownDragon_056473995.png",
        keywords: "art,blowing,glass,sculpture,thailand,torch"),
    Video(
        name: "Watercolor Your World",
        description:
            "Watch, learn, and be inspired by professional watercolorist, Sally Fischer,  in this video from her master class at the Orange Valley Contemporary Arts Museum. Sally is a master at painting the environment in which we live. Her brilliant watercolors, with their pronounced broad strokes and fluid tonality, have made her one of the most successful watercolorists of our time.",
        thumbnail:
            "http://orangevalleycaa.org/api/media/images/thumbs/ArtistWorking_059730538.png",
        keywords: "artist,color,painting,water,watercolor"),
    Video(
        name: "Pottery for Beginners",
        description:
            "The first of three in a series, this video covers the nuts and bolts of what you need to get started in creating your own pottery. Taught by versatile artist John Weber, who is known for his striking and detailed pottery, sculptures, and paintings.",
        thumbnail:
            "http://orangevalleycaa.org/api/media/images/thumbs/SpinningPottery_061544433.png",
        keywords: "clay,pottery,sculpture,throwing"),
    Video(
        name: "Plein Air Essentials",
        description:
            "If you've ever wanted to leave the confines of the studio, and paint the world \"en plein air\", as the French say, then this is the course for you. Join Albert Nowak, as he captivates learners with his engaging instruction, as well as his breadth and depth of knowledge of the art of plein air painting. ",
        thumbnail:
            "http://orangevalleycaa.org/api/media/images/thumbs/PleinAirArtist_061873487.png",
        keywords: "air,France,French,outdoors,painting,plein,watercolor"),
    Video(
        name: "The Marvels of Underwater Photography",
        description:
            "Underwater photography is a physically and mentally demanding art form, where the artist gets a fascinating glimpse into a rarely seen world.  Local photographer, Dennis Lipton, has been a leader in this field for 20 years. His photos have been published in National Geographic, American Scientific, Omni, and many more, and he's been showcased at MoMA and the Smithsonian.  Orange Valley Community Arts Association is happy to showcase this documentary that explores the growing field of underwater ",
        thumbnail:
            "http://orangevalleycaa.org/api/media/images/thumbs/UnderwaterPhoto_062314189.png",
        keywords: "diving,nature,ocean,photography,scuba,sea,underwater"),
    Video(
        name: "The Graffiti Movement",
        description:
            "Graffiti art is all the rage, and has been elevated from its back alley beginnings to large scale showcases on prominent buildings all over the world. Several Orange Valley artists are paving the way in this exciting and growing artistic field. This video shares their stories.",
        thumbnail:
            "http://orangevalleycaa.org/api/media/images/thumbs/GraffitiArtist_065000861.png",
        keywords: "art,graffiti,mural,painting,painting"),
    Video(
        name: "Glass Blowing 101",
        description:
            "Join OVCAA member, educator, and frequent lecturer, Roger Combs, for an introduction to the exciting art of glassblowing. In this engaging lesson, Roger explains the process of blowing a glass tube from start to finish. Students who are inspired by this video and would like to learn more, are encouraged to sign up for Roger's quarterly glassblowing workshops at the Orange Valley Community Center",
        thumbnail:
            "http://orangevalleycaa.org/api/media/images/thumbs/BlowingGlass_066874921.png",
        keywords: "blowing,glass,torch,tube")
  ];
}

// Video ne permettra d'avoir qu'une des 3 valeurs qu'il énumère
enum VideoSort {
  id, name, duration
  // pex ex. pour avoir le nom on fera VideoSort.name
}

// ajout de nouvelle fonctionnalité à l'enum VideoSort
extension ParseEnumName on VideoSort {
  String filterName() {
    // Si on a par ex. "VideoSort.duration"
    // on ne retourne que la partie à droite du point
    return toString().split('.').last;
  }
}