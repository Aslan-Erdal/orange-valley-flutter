import 'package:flutter/material.dart';
import 'package:orange_valley_caa/models/video.dart';
import 'package:orange_valley_caa/pages/details_page.dart';

// WIDGET AFFICHANT UNE GRIDVIEW
class VideosGrid extends StatelessWidget {
  final List<Video> videos;

  const VideosGrid({super.key, required this.videos});

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      crossAxisCount: 2,
      children: [
        for (var video in videos)
          GestureDetector(
            onTap: () {
              // print(video.name);
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => DetailsPage(video: video),
                  ));
            },
            child: _VideoTile(
              imageUrl: video.thumbnail,
              title: video.name,
            ),
          )
      ],
    );
  }
}

// WIDGET REPRESENTANT UN ELEMENT DE LA GRIDVIEW

class _VideoTile extends StatelessWidget {
  const _VideoTile({super.key, this.imageUrl, this.title});

  final title, imageUrl;

  @override
  Widget build(BuildContext context) {
    return Padding(
      // mettre de l'espace entre chaque élement de la GridView
      padding: const EdgeInsets.all(10.0),
      child: Stack(children: [
        // Image
        Hero(
          tag: imageUrl,
          child: Container(
            width: double.infinity,
            height: double.infinity,
            child: ClipRRect(
              // Pour avoir des rebords arrondis dans le container
              borderRadius: BorderRadius.circular(8.0),
              child: Image.network(imageUrl, fit: BoxFit.cover),
            ),
          ),
        ),
        // Titre
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            color: Colors.black.withOpacity(0.5),
            height: 35,
            child: Center(
              child: Text(
                title,
                textAlign: TextAlign.center,
                style: const TextStyle(
                    color: Colors.white, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        )
      ]),
    );
  }
}
