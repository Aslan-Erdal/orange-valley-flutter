import 'package:flutter/material.dart';
import 'package:orange_valley_caa/models/video.dart';
import 'package:orange_valley_caa/pages/video_player_page.dart';
import 'package:orange_valley_caa/utils/constants.dart';

// WIDGET REPRESANTANT LA PAGE DETAILS.
class DetailsPage extends StatelessWidget {
  const DetailsPage({super.key, required this.video});

  final Video video;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text('${video.name}'),
      ),
      body: Container(
        color: backgroundColor,
        height: double.infinity,
        child: _VideoDetail(video: video),
      ),
    );
  }
}

class _VideoDetail extends StatelessWidget {
  const _VideoDetail({super.key, required this.video});
  final Video video;

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        // Image du haut
        _DetailsPoster(
          posterUrl: video.thumbnail,
          videoUrl: video.videoUrl,
        ),
        // Séparation
        const SizedBox(
          height: 20,
        ),
        // description
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: _getDescription(video),
        ),
        // Séparation
        const SizedBox(height: 50),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: _DetailsKeywords(
            keywords: video.keywords,
          ),
        )
      ],
    );
  }
}

// WIDGET AFFICHANT L'IMAGE EN GRAND D'UNE VIDÉO
class _DetailsPoster extends StatelessWidget {

  final posterUrl;
  final videoUrl;

  const _DetailsPoster({super.key, this.posterUrl,this.videoUrl});

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: posterUrl,
      // Détection du clic et ouverture de la page
      child: GestureDetector(
        onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => VideoPlayerScreen(url: videoUrl),
          ),
        ),
        // Stack pour superposer l'imageet l'icône blanche
        child: Stack(
          children: [
            // image
            Container(
            width: double.infinity,
            height: 200,
            child: Image.network(
              posterUrl,
              fit: BoxFit.fitWidth,
            ),
          ),
          const Center(
            child: Icon(
            Icons.play_circle_outline,
            size: 200,
            color: Colors.white70
            ),
          )
          ]
        ),
      ),
    );
  }
}

// Method retournant la description d'une vidéo
Widget _getDescription(video) => Text(
      video.description,
      style: const TextStyle(
          fontSize: 20,
          color: Colors.white,
          fontFamily: 'CastoroCustom',
          fontStyle: FontStyle.italic),
    );

// Widget traitant les mots-clés de la vidéo
class _DetailsKeywords extends StatelessWidget {
  final keywords;

  const _DetailsKeywords({super.key, required this.keywords});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Wrap(
        children: [
          // Example de keywork renvoyé par l'appli:
          // "air France, French, outdoors, painting, plein, watercolor"
          for (var genre in keywords.split(','))
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: Chip(
                label: Text(genre),
              ),
            )
        ],
      ),
    );
  }
}
