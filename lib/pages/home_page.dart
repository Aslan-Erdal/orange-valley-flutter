import 'package:flutter/material.dart';
import 'package:orange_valley_caa/services/videos_api.dart';
import 'package:orange_valley_caa/utils/constants.dart';
import 'package:orange_valley_caa/utils/helper.dart';
import 'package:orange_valley_caa/widgets/videos_grid.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  var _selectedFilter = VideoSort.id;

  @override
  Widget build(BuildContext context) {
    getVideosFromApi().then(
      (value) => print(value.length),
    );
    return Scaffold(
      appBar: AppBar(
        title: const Center(child: Text('Orange Valley')),
        backgroundColor: Colors.black,
        actions: [
          PopupMenuButton(
            onSelected: _onChangeFilter,
            icon: const Icon(Icons.sort),
            offset: const Offset(0, 60),
            itemBuilder: (context) => [
            const PopupMenuItem(value: VideoSort.id, child: Text('Par défaut')),
            const PopupMenuItem(value: VideoSort.name, child: Text('Par nom')),
            const PopupMenuItem(value: VideoSort.duration, child: Text('Par durée')),
          ])
        ],
      ),
      body: Container(
        color: backgroundColor,
        child: FutureBuilder(
          future: getVideosFromApi(filter: _selectedFilter),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(child: CircularProgressIndicator());
            } else if (snapshot.hasError) {
              return Center(child: Text('Error : ${snapshot.error}'));
            } else {
              return VideosGrid(videos: snapshot.data!);
            }
          },
        ),
      ),
    );
  }

  void _onChangeFilter(VideoSort choice) {
    if (choice != _selectedFilter) {
      setState(() {
        _selectedFilter = choice;
      });
    }
  }

}


// import 'package:flutter/material.dart';
// import 'package:orange_valley_caa/models/video.dart';
// import 'package:orange_valley_caa/services/videos_api.dart';
// import 'package:orange_valley_caa/utils/constants.dart';
// import 'package:orange_valley_caa/utils/helper.dart';
// import 'package:orange_valley_caa/widgets/videos_grid.dart';

// class HomePage extends StatefulWidget {
//   const HomePage({super.key});

//   @override
//   State<HomePage> createState() => _HomePageState();
// }

// class _HomePageState extends State<HomePage> {

//   List<Video> videos = [];

//   @override
//   Widget build(BuildContext context) {
//     getVideosFromApi().then(
//       (value) {
//         setState(() {
//           videos = value;
//         });
//       },
//     );
//     return Scaffold(
//       appBar: AppBar(
//         title: const Center(child: Text('Orange Valley')),
//         backgroundColor: Colors.black,
//       ),
//       body: Container(
//         color: backgroundColor,
//         child: VideosGrid(videos: videos),
//       ),
//     );
//   }
// }
